# Messenger client for Distributed Systems course

A simple android messenger app that implements group chats communication.

<img src='assets/login.png' height='500'><img src='assets/chatlist.png' height='500'> <img src='assets/conversation.png' height='500'>

Uses <a href='https://github.com/InfJoker/helicopter/'>Helicopter</a> server (created by another team of course students) as a mean of messages transportation.

### Acknowledgement
This project is based on a template from the <a href='https://github.com/dytlabs/Messenger-Android-XML-Template'>Free android xml template for chat/messenger app</a> repository.