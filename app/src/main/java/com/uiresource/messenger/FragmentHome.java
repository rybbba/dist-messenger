package com.uiresource.messenger;

import android.app.AlarmManager;
import android.content.Intent;
import android.databinding.ObservableList;
import android.os.Bundle;
import android.os.StrictMode;
import android.support.annotation.DrawableRes;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.uiresource.messenger.recyclerview.Chat;
import com.uiresource.messenger.recyclerview.ChatAdapter;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

/**
 * Created by Dytstudio.
 */

public class FragmentHome extends Fragment implements ChatAdapter.ViewHolder.ClickListener{
    private RecyclerView mRecyclerView;
    private ChatAdapter mAdapter;

    private List<Chat> chatList;

    private TextView tv_selection;

    public FragmentHome(){
        setHasOptionsMenu(true);
    }
    public void onCreate(Bundle a){
        super.onCreate(a);
        setHasOptionsMenu(true);
    }
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_home, null, false);

        getActivity().supportInvalidateOptionsMenu();
        ((MainActivity)getActivity()).changeTitle(R.id.toolbar, "Messages");

        tv_selection = (TextView) view.findViewById(R.id.tv_selection);
        mRecyclerView = (RecyclerView) view.findViewById(R.id.recyclerView);
        mRecyclerView.setHasFixedSize(true);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        State.updateMessageList();
        chatList = setData();
        mAdapter = new ChatAdapter(getContext(), chatList,this);
        mRecyclerView.setAdapter (mAdapter);

        State.messageList.addOnListChangedCallback(new ObservableList.OnListChangedCallback<ObservableList<Message>>() {
            @Override
            public void onChanged(ObservableList<Message> sender) {}

            @Override
            public void onItemRangeChanged(ObservableList<Message> sender, int positionStart, int itemCount) {}

            @Override
            public void onItemRangeInserted(ObservableList<Message> sender, int positionStart, int itemCount) {
                chatList = setData();
                mAdapter.setList(chatList);
                Log.d("FragmentHome", "Chat list updated");
            }

            @Override
            public void onItemRangeMoved(ObservableList<Message> sender, int fromPosition, int toPosition, int itemCount) {}

            @Override
            public void onItemRangeRemoved(ObservableList<Message> sender, int positionStart, int itemCount) {}
        });

        return view;
    }
    public List<Chat> setData(){

        List<Chat> data = new ArrayList<>();

        for (Message msg : State.messageList){
            if (msg.parentId != State.root) {
                continue;
            }

            Chat chat = new Chat();
            java.util.Date time = new java.util.Date(msg.timestamp * 1000);
            //chat.setmTime(time.toString());
            chat.setName(Long.toString(msg.messageId));

            chat.setImage(R.drawable.group);
            chat.setOnline(false);
            //chat.setLastChat("");

            data.add(chat);
        }
        return data;
    }

    @Override
    public void onItemClicked (int position) {
        Chat chat = chatList.get(position);
        Intent intent = new Intent(getActivity(), Conversation.class);
        intent.putExtra("chatRoot", chat.getName());
        startActivity(intent);
    }

    @Override
    public boolean onItemLongClicked (int position) {
        toggleSelection(position);
        return true;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        return false;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.menu_edit, menu);

        MenuItem createChatMenuItem = menu.findItem(R.id.new_chat);
        createChatMenuItem.setOnMenuItemClickListener(item -> {
            // create a new chat
            Message msg = new Message();
            msg.setParentId(State.root);
            msg.setText("Chat created");
            msg.setTimestamp(System.currentTimeMillis() / 1000L);
            msg.setUserId(State.userId);
            String chatId = Long.toString(State.postMessage(msg));

            State.updateMessageList();
            //chatList = setData();
            //mAdapter.setList(chatList);

            // open the conversation intent activity for the new chat
            Chat chat = chatList.stream()
                    .filter(listItem -> chatId.equals(listItem.getName()))
                    .findAny()
                    .orElse(null);
            if (chat == null) {
                Log.e("Home", "Chat was not created");
                return false;
            }
            Intent intent = new Intent(getActivity(), Conversation.class);
            intent.putExtra("chatRoot", chat.getName());
            startActivity(intent);

            return true;
        });
    }

    /*
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        menu.clear();
        inflater.inflate(R.menu.menu_edit, menu);
    }
    */

    private void toggleSelection(int position) {
        mAdapter.toggleSelection (position);
        if (mAdapter.getSelectedItemCount()>0){
            tv_selection.setVisibility(View.VISIBLE);
        }else
            tv_selection.setVisibility(View.GONE);


        getActivity().runOnUiThread(() -> tv_selection.setText("Delete ("+mAdapter.getSelectedItemCount()+")"));

    }
}
