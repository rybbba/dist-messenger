package com.uiresource.messenger;

import android.databinding.ObservableArrayList;
import android.databinding.ObservableList;
import android.os.Handler;
import android.os.StrictMode;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;

// How to end this cringe rightly (with hardcoded configuration variables):
public class State {
    private static final Handler handler = new Handler();
    private static final Runnable runnable = new Runnable() {
        @Override
        public void run() {
            updateMessageList();
            handler.postDelayed(runnable, 1000);
        }
    };

    private static String privateKey;
    static void setPrivateKey(String pk) {
        privateKey = pk;
    };

    static {
        // Clown mode ON
        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);
        // Start the Runnable immediately
        handler.post(runnable);
    }

    public static String userId = "AAAAC3NzaC1lZDI1NTE5AAAAIPQFJeNbrZXOkvPaW34Dneb+XDVkMF85VvSP20yr/uqO";
    public static WebClient client = new WebClient("51.250.87.22", 8288);
    public static int root = 16777216;

    public static ObservableList<Message> messageList = new ObservableArrayList<>();

    public static boolean updateMessageList() {
        try {
            Log.d("UpdateMessageList", "Getting messages");
            State.messageList.clear();
            State.messageList.addAll(client.getAllMessages(State.root));
            return true;
        } catch (java.io.IOException e) {
            Log.e("UpdateMessageList", "Encountered exception", e);
            return false;
        }
    }

    public static long postMessage(Message msg) {
        try {
            Log.d("State", "Sending message");
            return State.client.postMessage(msg);
        } catch (java.io.IOException e) {
            Log.e("State", "Encountered exception", e);
            return -1;
        }
    }
}
