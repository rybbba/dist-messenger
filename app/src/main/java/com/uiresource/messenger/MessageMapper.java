package com.uiresource.messenger;

import android.util.Base64;

import com.fasterxml.jackson.annotation.*;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;

public class MessageMapper {
    static ObjectMapper objectMapper = new ObjectMapper();

    // parentId can be left empty
    static public String serializePostMessage(Message msg) throws JsonProcessingException {
        Content content = new Content();
        content.setUserId(msg.getUserId());
        content.setText(msg.getText());
        content.setTimestamp(msg.getTimestamp());
        String contentString = objectMapper.writeValueAsString(content);
        contentString = Base64.encodeToString(contentString.getBytes(StandardCharsets.UTF_8), Base64.DEFAULT);

        ServerMessage serverMessage = new ServerMessage();
        serverMessage.setRef(Long.toString(msg.getParentId()));
        serverMessage.setContent(contentString);
        return objectMapper.writeValueAsString(serverMessage);
    }

    // We usually only need messageId, but will deserialize all fields just in case
    static public Message deserializeMessage(String json) throws JsonProcessingException {
        Message message = new Message();

        ServerMessage serverMessage = objectMapper.readValue(json, ServerMessage.class);
        message.setMessageId(Long.parseLong(serverMessage.getLseq()));
        message.setParentId(Long.parseLong(serverMessage.getRef()));

        String contentJson = new String(Base64.decode(serverMessage.content, Base64.DEFAULT), StandardCharsets.UTF_8);
        Content content = objectMapper.readValue(contentJson, Content.class);
        message.setUserId(content.getUserId());
        message.setText(content.getText());
        message.setTimestamp(content.getTimestamp());

        return message;
    }

    static public List<Message> deserializeMessages(String json) throws JsonProcessingException {
        ServerMessage[] serverMessages = objectMapper.readValue(json, ServerMessage[].class);
        List<Message> messages = new ArrayList<>();
        for (ServerMessage serverMessage : serverMessages) {
            Message message = new Message();
            message.setMessageId(Long.parseLong(serverMessage.getLseq()));
            message.setParentId(Long.parseLong(serverMessage.getRef()));

            String contentJson = new String(Base64.decode(serverMessage.content, Base64.DEFAULT), StandardCharsets.UTF_8);
            Content content = objectMapper.readValue(contentJson, Content.class);
            message.setUserId(content.getUserId());
            message.setText(content.getText());
            message.setTimestamp(content.getTimestamp());

            messages.add(message);
        }


        return messages;
    }
}


class ServerMessage {
    String lseq;
    String ref;
    String content;

    @JsonIgnore  // Ignore lseq when serializing
    public String getLseq() {
        return lseq;
    }

    @JsonProperty // Force ref deserialization
    public void setLseq(String lseq) {
        this.lseq = lseq;
    }

    public String getRef() {
        return ref;
    }

    public void setRef(String ref) {
        this.ref = ref;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }
}


class Content {
    String userId;
    String text;
    long timestamp;

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public long getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(long timestamp) {
        this.timestamp = timestamp;
    }
}
