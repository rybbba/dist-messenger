package com.uiresource.messenger;

import android.os.Build;
import android.support.annotation.RequiresApi;
import android.util.Log;


import java.io.IOException;
import java.io.StringReader;
import java.nio.charset.StandardCharsets;
import java.util.Base64;

import org.bouncycastle.crypto.CryptoException;
import org.bouncycastle.crypto.Signer;
import org.bouncycastle.crypto.params.AsymmetricKeyParameter;
import org.bouncycastle.crypto.signers.Ed25519Signer;
import org.bouncycastle.crypto.util.OpenSSHPrivateKeyUtil;
import org.bouncycastle.crypto.util.OpenSSHPublicKeyUtil;
import org.bouncycastle.util.io.pem.PemReader;

public class ClientKeyVerification {
    @RequiresApi(api = Build.VERSION_CODES.O)
    public static boolean checkKeyMatch(String publicKeyString, String privateKeyString) {
        try {
            // These are correct example keys!

//            publicKeyString = "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIPA/EJrTPNuHUH7FLreHcmgi/ejE44Dt6WvNpFq/6v7Z noname\n";
//            privateKeyString = "-----BEGIN OPENSSH PRIVATE KEY-----\n" +
//                    "b3BlbnNzaC1rZXktdjEAAAAABG5vbmUAAAAEbm9uZQAAAAAAAAABAAAAMwAAAAtzc2gtZW\n" +
//                    "QyNTUxOQAAACDwPxCa0zzbh1B+xS63h3JoIv3oxOOA7elrzaRav+r+2QAAAJB/cFzZf3Bc\n" +
//                    "2QAAAAtzc2gtZWQyNTUxOQAAACDwPxCa0zzbh1B+xS63h3JoIv3oxOOA7elrzaRav+r+2Q\n" +
//                    "AAAECEDYzemkiIBv1yB9Kdh1L4K63yQms3XPt7VY6vfEjzafA/EJrTPNuHUH7FLreHcmgi\n" +
//                    "/ejE44Dt6WvNpFq/6v7ZAAAABm5vbmFtZQECAwQFBgc=\n" +
//                    "-----END OPENSSH PRIVATE KEY-----\n";

            AsymmetricKeyParameter privateKey = parseStringToPrivateKey(privateKeyString);
            AsymmetricKeyParameter publicKey = parseStringToPublicKey(publicKeyString);

            return runKeyPairVerification(publicKey, privateKey);
        } catch (Exception ex) {
            Log.e("Auth", "Auth error: " + ex.getMessage());
            return false;
        }
    }

    public static AsymmetricKeyParameter parseStringToPrivateKey(String privateKeyString) throws IOException {
        try (StringReader stringReader = new StringReader(privateKeyString);
             PemReader pemReader = new PemReader(stringReader)) {
            byte[] privateKeyContent = pemReader.readPemObject().getContent();
            return OpenSSHPrivateKeyUtil.parsePrivateKeyBlob(privateKeyContent);
        }
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    public static AsymmetricKeyParameter parseStringToPublicKey(String publicKeyString) {
        String publicKeyBody = publicKeyString.split(" ")[1];
        return OpenSSHPublicKeyUtil.parsePublicKey(Base64.getDecoder().decode(publicKeyBody));
    }

    public static boolean runKeyPairVerification(AsymmetricKeyParameter publicKey,
                                                 AsymmetricKeyParameter privateKey) throws CryptoException {
        byte[] challenge = "This is a challenge message".getBytes(StandardCharsets.UTF_8);
        Signer signer = new Ed25519Signer();
        signer.init(true, privateKey);
        signer.update(challenge, 0, challenge.length);
        byte[] signature = signer.generateSignature();
        Signer verifier = new Ed25519Signer();
        verifier.init(false, publicKey);
        verifier.update(challenge, 0, challenge.length);
        return verifier.verifySignature(signature);
    }
}
