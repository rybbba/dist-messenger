package com.uiresource.messenger;

import android.content.Intent;
import android.os.Bundle;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class SetupKeys extends BaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_setup_client);
        Button clear = (Button) findViewById(R.id.bt_clear);
        clear.setOnClickListener(view -> {
            EditText publicKeyField = (EditText) findViewById(R.id.et_public_password);
            EditText privateKeyField = (EditText) findViewById(R.id.et_private_password);
            publicKeyField.getText().clear();
            privateKeyField.getText().clear();
        });

        Button button = (Button) findViewById(R.id.bt_continue);

        button.setOnClickListener(view -> {
            EditText publicKeyField = (EditText) findViewById(R.id.et_public_password);
            String publicKey = publicKeyField.getText().toString();
            EditText privateKeyField = (EditText) findViewById(R.id.et_private_password);
            String privateKey = privateKeyField.getText().toString();

            boolean keysMatch = ClientKeyVerification.checkKeyMatch(publicKey, privateKey);

            if (keysMatch) {
                State.setPrivateKey(privateKey);
                State.userId = publicKey.split(" ")[1]; // publicKey format: "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIPA/EJrTPNuHUH7FLreHcmgi/ejE44Dt6WvNpFq/6v7Z noname\n";
                startActivity(new Intent(SetupKeys.this, MainActivity.class));
            } else {
                Toast.makeText(SetupKeys.this, "Public and private keys do not match", Toast.LENGTH_SHORT).show();
            }
        });

        Button button_wok = (Button) findViewById(R.id.bt_continue_wok);
        button_wok.setOnClickListener(view -> {
            EditText publicKeyField = (EditText) findViewById(R.id.et_public_password);
            String publicKey = publicKeyField.getText().toString();
            State.userId = publicKey.split(" ")[1]; // publicKey format: "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIPA/EJrTPNuHUH7FLreHcmgi/ejE44Dt6WvNpFq/6v7Z noname\n"
            startActivity(new Intent(SetupKeys.this, MainActivity.class));
        });
    }
}
