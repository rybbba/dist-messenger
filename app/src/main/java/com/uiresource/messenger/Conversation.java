package com.uiresource.messenger;

import android.databinding.ObservableList;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.uiresource.messenger.recyclerview.Chat;
import com.uiresource.messenger.recylcerchat.ChatData;
import com.uiresource.messenger.recylcerchat.ConversationRecyclerView;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;


public class Conversation extends BaseActivity {
    private RecyclerView mRecyclerView;
    private ConversationRecyclerView mAdapter;
    private EditText text;
    private Button send;

    private long chatRoot;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_conversation);

        chatRoot = Long.parseLong(getIntent().getStringExtra("chatRoot"));

        setupToolbarWithUpNav(R.id.toolbar, getIntent().getStringExtra("chatRoot"), R.drawable.ic_action_back);

        mRecyclerView = (RecyclerView) findViewById(R.id.recyclerView);
        mRecyclerView.setHasFixedSize(true);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        State.updateMessageList();
        mAdapter = new ConversationRecyclerView(this, setData());
        mRecyclerView.setAdapter(mAdapter);
        mRecyclerView.postDelayed(() -> mRecyclerView.smoothScrollToPosition(mRecyclerView.getAdapter().getItemCount() - 1), 1000);

        text = (EditText) findViewById(R.id.et_message);
        text.setOnClickListener(view -> mRecyclerView.postDelayed(
                () -> mRecyclerView.smoothScrollToPosition(mRecyclerView.getAdapter().getItemCount() - 1), 500)
        );
        send = (Button) findViewById(R.id.bt_send);
        send.setOnClickListener(view -> {
            if (!Objects.equals(text.getText().toString(), "")){
                Message msg = new Message();
                msg.setParentId(chatRoot);
                msg.setUserId(State.userId);
                msg.setText(text.getText().toString());
                msg.setTimestamp(System.currentTimeMillis() / 1000L);
                State.postMessage(msg);

                State.updateMessageList();
                /*
                List<ChatData> data = new ArrayList<>();
                ChatData item = new ChatData();
                item.setTime("6:00pm");
                item.setType("2");
                item.setText(text.getText().toString());
                data.add(item);
                mAdapter.addItem(data);
                 */
                text.setText("");
            }
        });

        // TODO: remove callback on activity destruction(?)
        State.messageList.addOnListChangedCallback(new ObservableList.OnListChangedCallback<ObservableList<Message>>() {
            @Override
            public void onChanged(ObservableList<Message> sender) {}

            @Override
            public void onItemRangeChanged(ObservableList<Message> sender, int positionStart, int itemCount) {}

            @Override
            public void onItemRangeInserted(ObservableList<Message> sender, int positionStart, int itemCount) {
                mAdapter.setItems(setData());
                mRecyclerView.smoothScrollToPosition(mRecyclerView.getAdapter().getItemCount() -1);
                Log.d("Converation", "Chat updated");
            }

            @Override
            public void onItemRangeMoved(ObservableList<Message> sender, int fromPosition, int toPosition, int itemCount) {}

            @Override
            public void onItemRangeRemoved(ObservableList<Message> sender, int positionStart, int itemCount) {}
        });
    }

    public List<ChatData> setData(){
        List<ChatData> data = new ArrayList<>();

        ChatData rootMessage = new ChatData();
        rootMessage.setType("0");
        rootMessage.setText("Chat created");
        data.add(rootMessage);

        for (Message msg : State.messageList){
            if (msg.getParentId() != chatRoot) {
                continue;
            }
            ChatData item = new ChatData();
            if (Objects.equals(msg.getUserId(), State.userId)) {
                item.setType("2");
            } else {
                item.setType("1");
            }
            item.setSender(msg.userId);
            item.setText(msg.text);
            java.util.Date date = new java.util.Date(msg.timestamp * 1000);
            //item.setTime(date.toString());
            item.setTime("");
            data.add(item);
        }

        return data;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        //inflater.inflate(R.menu.menu_userphoto, menu);
        return true;
    }
}
