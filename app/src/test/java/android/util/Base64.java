package android.util;

import java.nio.charset.StandardCharsets;

public class Base64 {
    // :clown:

    public static String encodeToString(byte[] input, int flags) {
        return new String(input, StandardCharsets.UTF_8);
    }

    public static byte[] decode(String str, int flags) {
        return str.getBytes(StandardCharsets.UTF_8);
    }

}