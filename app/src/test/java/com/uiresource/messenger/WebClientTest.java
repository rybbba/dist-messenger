package com.uiresource.messenger;

import org.junit.Test;

import static org.junit.Assert.*;

//
// THESE TESTS ARE NOT WORKING BECAUSE OF THE BASE64 PROBLEM
//

public class WebClientTest {
    @Test
    public void can_get_messages() throws Exception {
        WebClient client = new WebClient("51.250.87.22", 8288);
        System.out.println(client.getMessages(16777216, 16777216));
    }

    @Test
    public void can_post_messages() throws Exception {
        WebClient client = new WebClient("51.250.87.22", 8288);
        Message msg = new Message();
        msg.setParentId(0);
        msg.setUserId("");
        msg.setText("Messenger created");
        msg.setTimestamp(0);

        System.out.println(client.postMessage(msg));
    }
}
