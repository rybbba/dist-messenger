package com.uiresource.messenger;

import org.junit.Test;

import static org.junit.Assert.*;

public class MessageMapperTest {
    @Test
    public void message_serialization_matches_deserialization() throws Exception {
        Message message = new Message();
        message.setParentId(0);
        message.setUserId("AAAAC3NzaC1lZDI1NTE5AAAAIPQFJeNbrZXOkvPaW34Dneb+XDVkMF85VvSP20yr/uqO");
        message.setText("Hello World!");
        message.setTimestamp(1994);

        String serialized = MessageMapper.serializePostMessage(message);

        assertFalse(serialized.contains("lseq"));

        String toDeserialize = "{\"lseq\":\"13\"," + serialized.substring(1);
        Message newMessage = MessageMapper.deserializeMessage(toDeserialize);

        assertEquals(13, newMessage.getMessageId());
        assertEquals(message.getParentId(), newMessage.getParentId());
        assertEquals(message.getUserId(), newMessage.getUserId());
        assertEquals(message.getText(), newMessage.getText());
        assertEquals(message.getTimestamp(), newMessage.getTimestamp());
    }
}